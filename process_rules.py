__author__ = 'Sirenko_Viktor'
import mmap
from tools import CColumnKey, CArgsForWorker
import multiprocessing as mp
from main_worker import CMainWorker

# col_val_slice = {"yearmonthday": (0, 10), "yearmonthdayhour": (0, 13)}
headers_t_data = ("transaction_id", "date", "action", "banner_id", "browser", "device")
headers_t_add = ("banner_id", "banner_name")


class CProcessRules(object):
    __slots__ = ("_args", "_queue")

    def __init__(self, args):
        self._args = args
        self._queue = mp.Queue()

    def process_rules(self):
        with open(self._args.path_to_rules, "r") as rules_file:
            rules_list = rules_file.readlines()

            for rule in rules_list:
                rule_parameters = rule.split()
                columns = []
                for parameter in rule_parameters:
                    column_obj = self._columns_to_process(parameter.split(","), rule)
                    if column_obj:
                        columns.append(column_obj)
                    else:
                        continue

                # columns[0].is_slice = True
                # columns[0].val_slice = col_val_slice[rule_parameters[0]]

                worker_args_obj = CArgsForWorker(columns, self._args.path_to_res_dir+rule.rstrip(),
                                                 self._args.counting_values, self._args.counting_values_col)
                self._queue.put(worker_args_obj)

            self._start_workers(rules_list)

    def _columns_to_process(self, parameter, rule):
        """create column object with parameters like foreign table relations dict or slice values"""
        if parameter[0] in headers_t_data:
            return CColumnKey(parameter, headers_t_data.index(parameter[0]))

        for foreign_table in self._args.path_to_foreign_table:
            # headers_t_add = csv.reader(open(foreign_table, "r")).next()
            if parameter[0] in headers_t_add:
                foreign_key_name = set(headers_t_add).intersection(set(headers_t_data)).pop()
                col_numb_tuple = (headers_t_add.index(foreign_key_name), headers_t_add.index(parameter[0]))
                return CColumnKey(parameter, headers_t_data.index(foreign_key_name), True, foreign_table, col_numb_tuple)
        else:
            print "Parameter \"{0}\" not found for rule: {1}".format(parameter[0], rule)
        return False

    def _start_workers(self, rules_list):
        """create workers for processing queue"""
        processes = []
        processes_amount = range(mp.cpu_count()) if mp.cpu_count() <= len(rules_list) else range(len(rules_list))

        with open(self._args.path_to_data, "r+b") as rawdata_f:
            mmap_file = mmap.mmap(rawdata_f.fileno(), 0, access=mmap.ACCESS_READ)
            for i in processes_amount:
                process = CMainWorker(self._queue, mmap_file)
                process.daemon = True
                processes.append(process)
                process.start()

            for process in processes:
                process.join()