__author__ = 'Sirenko_Viktor'
import csv
import time
import multiprocessing as mp
from Queue import Empty

DELIMITER = ","


class CMainWorker(mp.Process):
    __slots__ = ("_mmap_file", "_queue", "_args", "_res_csv_obj")

    def __init__(self, queue, mmap_file):
        super(CMainWorker, self).__init__()
        self._mmap_file = mmap_file
        self._queue = queue
        self._args = None
        self._res_csv_obj = None

    def run(self):
        """process file row by row. first key must be sorted"""
        while not self._queue.empty():
            try:
                self._args = self._queue.get(block=True, timeout=0.5)
            except Empty:
                break

            time_start = time.time()
            print "Start rule \"{0}\"".format(self._args.res_file_name)
            with open(self._args.res_file_name+".csv", "w+b") as res_file:
                self._res_csv_obj = csv.writer(res_file, delimiter=DELIMITER)
                output_data = {}
                prev_first_key = None
                self._mmap_file.seek(0)
                for row in iter(self._mmap_file.readline, ""):
                    row = row.rstrip().split(DELIMITER)
                    if not row:
                        continue

                    try:
                        counting_value = row[self._args.counting_values_col]
                        if counting_value not in self._args.counting_values:
                            continue
    
                        columns_to_group = []
                        for column in self._args.columns:
                            if column.is_slice:
                                columns_to_group.append(row[column.col_numb][column.val_slice[0]:column.val_slice[1]])
                            else:
                                columns_to_group.append(row[column.col_numb])
                    except IndexError:
                        continue
                        
                    first_key = columns_to_group[0]
                    if prev_first_key != first_key:
                        self._save_data_to_file(output_data, [], 0)
                        output_data = {}
                        output_data[first_key] = {}

                    next_incl_col = output_data[first_key]
                    for column in columns_to_group[1:]:
                        if column in next_incl_col:
                            next_incl_col = next_incl_col[column]
                        else:
                            next_incl_col[column] = {}
                            next_incl_col = next_incl_col[column]

                    if counting_value in next_incl_col:
                        next_incl_col[counting_value] += 1
                    else:
                        next_incl_col[counting_value] = {}
                        next_incl_col[counting_value] = 1

                    prev_first_key = first_key
                else:
                    self._save_data_to_file(output_data, [], 0)
                print "Rule \"{0}\" is over. Rule worked: {1} sec".format(self._args.res_file_name, time.time()-time_start)

    def _save_data_to_file(self, output_data, cols_list, col_index):
        """creating row for csv result file"""
        if col_index == len(self._args.columns) - 1:
            for col in output_data:
                col_data = self._name_for_col(col_index, col)

                counting_val_dict = output_data[col]
                counting_val_list = [counting_val_dict.get(val, 0) for val in self._args.counting_values]

                str_out = cols_list[:]
                str_out.append(col_data)
                str_out.extend(counting_val_list)
                self._res_csv_obj.writerow(str_out)
        else:
            for col in output_data:
                col_data = self._name_for_col(col_index, col)
                cols_list.append(col_data)
                self._save_data_to_file(output_data[col], cols_list, col_index+1)
                cols_list.pop()

    def _name_for_col(self, index, col_val):
        """getting name for id from foreign table"""
        return self._args.columns[index].relations_dict.get(col_val, "Unknown ID: "+col_val) if self._args.columns[index].is_foreign else col_val