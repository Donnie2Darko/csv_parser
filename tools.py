__author__ = 'Sirenko_Viktor'
import csv
from functools import wraps


def cache_relations_dict(func):
    cache_rel = {}
    @wraps(func)
    def wrapper(*args):
        if args[1:] in cache_rel:
            return cache_rel[args[1:]]
        else:
            result = func(*args)
            cache_rel[args[1:]] = result
            return result
    return wrapper


class CColumnKey(object):
    __slots__ = ('name', 'col_numb', 'is_foreign', 'is_slice', 'relations_dict', 'val_slice')

    def __init__(self, column, col_numb, is_foreign=False, path_to_table=None, col_tuple=None):
        self.name = column[0]
        self.col_numb = col_numb
        self.is_foreign = is_foreign
        self.is_slice = True if len(column) > 2 else False
        if is_foreign:
            self.relations_dict = self._create_relations_dict(path_to_table, col_tuple)
        if self.is_slice:
            self.val_slice = (int(column[1]), int(column[2]))

    @cache_relations_dict
    def _create_relations_dict(self, path_to_table, col_tuple):
        result = {}
        with open(path_to_table, "r") as file_obj:
            csv_obj = csv.reader(file_obj)
            for row in csv_obj:
                if row:
                    result[row[col_tuple[0]]] = row[col_tuple[1]]
        return result


class CArgsForWorker(object):
    __slots__ = ('columns', 'res_file_name', 'counting_values', 'counting_values_col')

    def __init__(self, columns, res_file_name, counting_values, counting_values_col):
        self.columns = columns
        self.res_file_name = res_file_name
        self.counting_values = counting_values
        self.counting_values_col = counting_values_col