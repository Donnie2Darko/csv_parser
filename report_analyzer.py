__author__ = 'Sirenko_Viktor'
import time
import argparse
from process_rules import CProcessRules

if __name__ == "__main__":
    time_start = time.time()
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument("--path_to_rules", "-ptr", default="rule.txt",
                            help="path to file with rules")
        parser.add_argument("--path_to_data", "-ptd", default="rawdatalite.csv",
                            help="path to data file")
        parser.add_argument("--path_to_foreign_table", "-ptf", nargs="*", default=("lookup.csv",),
                            help="path to foreign(s) table(s) files separated by space")
        parser.add_argument("--path_to_res_dir", "-ptrd", default="", help="path to result dir")
        parser.add_argument("--counting_values", "-cv", nargs="*", default=("show", "request", "click"),
                            help="value(s), which need to count, separated by space")
        parser.add_argument("--counting_values_col", "-cvc", default=2,
                            help="column number, which need to count", type=int)

        args = parser.parse_args()
        CProcessRules(args).process_rules()
    except KeyboardInterrupt:
        print "canceling.."
    finally:
        t2 = time.time()
        print "Task worked: {} sec".format(time.time()-time_start)